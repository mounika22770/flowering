def start():
    global maze
    maze=[[0 for i in range(12)] for j in range(12)]#for 10X10 matrix ,we need 12X12 matrix
    for i in range(12):
       maze[i][0]=-1
       maze[i][12]=-1
    for i in range(12):
        maze[0][i]=-1
        maze[12][i]=-1
    

def neighbours(i,j):
    return([(i-1,j),(i,j+1),(i+1,j),(i,j-1)])
    
                  
def find(i,j):
    global maze,stack
    n=neighbours(i,j)#getting neighbours
    for a,b in n:
        if maze[a][b]=="G": #goal reached
            stack.append((a,b))
            return True
        if  maze[a][b]==0:
            stack.append((a,b))
            maze[a][b]="visited"
            extend=find(a,b)
            if extend:
            
                    return True
            else:
                    stack.pop()
                    maze[a][b]="R"#Rejected
    else:
        return False
    return True
def path():#marking the path from "goal" to "start" as "P" except start and goal boxes
    global stack,maze
    x,y=stack.pop()
    for i in range(len(stack)-1):
       x,y=stack.pop()
       maze[x][y]="p"

def printMaze():#printing the maze
    global maze
    for i in range(1,11):
        for j in range(1,11):
            print(maze[i][j],end=" ")
        print("\n")
    
def blockedBoxes():
    global maze
    maze[1][2]=-1#indicates obstacle ,we can't enter into that box
    maze[1][3]=-1
    maze[1][4]=-1
    maze[1][8]=-1
    maze[2][2]=-1
    maze[2][3]=-1
    maze[2][4]=-1
    maze[2][6]=-1
    maze[2][8]=-1
    maze[3][4]=-1
    maze[3][6]=-1
    maze[4][2]=-1
    maze[4][4]=-1
    maze[4][6]=-1
    maze[4][7]=-1
    maze[4][8]=-1
    maze[4][9]=-1
    maze[5][2]=-1
    maze[5][4]=-1
    maze[5][6]=-1
    maze[5][8]=-1
    maze[6][2]=-1
    maze[6][3]=-1
    maze[6][4]=-1
    maze[6][6]=-1
    maze[6][8]=-1
    maze[6][10]=-1
    maze[7][2]=-1
    maze[7][6]=-1
    maze[7][8]=-1
    maze[7][10]=-1
    maze[8][2]=-1
    maze[8][4]=-1
    maze[8][5]=-1
    maze[8][6]=-1
    maze[8][8]=-1
    maze[9][8]=-1
    maze[10][5]=-1
    maze[10][6]=-1
    maze[10][7]=-1
    maze[10][8]=-1
maze=[]
start()
sr,sc=0,0
stack=[(sr+1,sc+1)]
maze[sr+1][sc+1]="S"
gr,gc=9,9#goal is at (9,9) in 10X10,but at (10,10) in 12X12
maze[gr+1][gc+1]="G"#
#obstacles
blockedBoxes()
find(sr+1,sc+1)
path()
blockedBoxes()
printMaze()


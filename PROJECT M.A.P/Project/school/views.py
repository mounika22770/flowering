from django.shortcuts import render
from django.shortcuts import redirect
from django.http import HttpResponseRedirect, HttpResponse

from .forms import ExamModelForm, ExamFormset, QuestionFormset
from .models import Question

def create_exam(request):
	if request.method == 'POST':
		form = ExamModelForm(request.POST)
		if form.is_valid():
			form.save()
			return HttpResponseRedirect('/only exam is created')
	else:
		form = ExamModelForm()
	return render(request, 'school/create_exam.html', {'form': form})

def create_exam_with_questions(request):
    template_name = 'school/create_exam_with_questions.html'
    if request.method == 'GET':
        examform = ExamModelForm(request.GET or None)
        formset = QuestionFormset(queryset=Question.objects.none())
    elif request.method == 'POST':
        examform = ExamModelForm(request.POST)
        formset = QuestionFormset(request.POST)
        if examform.is_valid() and formset.is_valid():
            # first save this exam, as its reference will be used in `Question`
            exam = examform.save()
            for form in formset:
                # so that `question` instance can be attached.
                question = form.save(commit=False)
                question.exam = exam 
                question.save()
            return redirect("map:map-home")
    return render(request, template_name, {
        'examform': examform,
        'formset': formset,
    })

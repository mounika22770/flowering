from django.urls import path

from .views import create_exam, create_exam_with_questions

app_name = 'school'
urlpatterns = [
    path("create_exam/", create_exam, name="school-create-exam"),
    path("create_exam_with_questions/", create_exam_with_questions, name="create_exam_with_questions"),
]


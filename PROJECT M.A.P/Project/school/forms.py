from django import forms
from django.forms import (formset_factory, modelformset_factory)
from .models import Exam, Question

class ExamModelForm(forms.ModelForm):
	class Meta:
		model = Exam
		fields = ['name', 'section', 'duration', 'subject', 'start', 'teacher']


ExamFormset = modelformset_factory(
    Exam,
    fields=('name', 'section', 'duration', 'subject', 'start', 'teacher', ),
    extra=1,
    widgets={
        'name': forms.TextInput(attrs={
            'class': 'form-control',
            'placeholder': 'Enter exam Name here'
            }),
        'section': forms.TextInput(attrs ={
            'class': 'form-control',
            'placeholder': 'Enter section'
            }),
        'duration': forms.TextInput(attrs ={
            'class': 'form-control',
            'placeholder': 'Enter duration'
            }),
        'subject': forms.TextInput(attrs ={
            'class': 'form-control',
            'placeholder': 'Enter subject'
            }),
        'start': forms.TextInput(attrs ={
            'class': 'form-control',
            'placeholder': 'Enter start date time'
            }),
        'teacher': forms.TextInput(attrs ={
            'class': 'form-control',
            'placeholder': 'Enter teacher here'
            }),

    }
)

QuestionFormset = modelformset_factory(
    Question,
    fields=('question', 'ans', 'op2', 'op3', 'op4', ),
    extra=1,
    widgets={'question': forms.TextInput(attrs={
            'class': 'form-control',
            'placeholder': 'Enter Question here'
        }),
    'ans': forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Enter answer here'
        }),
    'op2': forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Enter option2 here'
        }),
    'op3': forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Enter option3 here'
        }),
    'op4': forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Enter option4 here'
        }),
    }
)









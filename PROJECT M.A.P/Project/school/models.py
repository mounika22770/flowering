from django.db import models


# class Section(models.Model):
# 	name = models.CharField(max_length = 20)



# class Student(models.Model):
# 	name = models.CharField(max_length = 20)
# 	roll = models.CharField(primary_key = True, max_length = 20)
# 	section = models.ForeignKey(Section, on_delete = models.DO_NOTHING)



# class Subject(models.Model):
# 	name = models.CharField(max_length = 20)
# 	section = models.
# 	teacher = 
	

# class Teacher(models.Model):
# 	name = models.CharField(max_length = 20)
# 	uid = models.CharField(max_length = 20, primary_key = True)
# 	password = 
# 	subjects = 
# 	sections = 

class School(models.Model):
	name =models.CharField(max_length = 20)
	#uid = models.CharField(max_length = 20)
	address = models.TextField(max_length = 20)

	def __str__(self):
		return self.name

class Section(models.Model):
	name = models.CharField(max_length = 20)
	school = models.ForeignKey(School, on_delete = models.CASCADE)

	def __str__(self):
		return self.name

class Subject(models.Model):
	name = models.CharField(max_length = 20)
	section = models.ForeignKey(Section, on_delete = models.CASCADE)

	def __str__(self):
		return self.name


class Exam(models.Model):
	name = models.CharField(max_length = 20)
	section = models.ForeignKey(Section, on_delete = models.CASCADE)
	subject = models.ForeignKey(Subject, on_delete = models.CASCADE)
	duration = models.TimeField()
	start = models.DateTimeField()
	teacher = models.CharField(max_length = 20)

	def __str__(self):
		return self.name


class Question(models.Model):
	question = models.TextField(max_length = 20)
	ans = models.CharField(max_length = 20)
	op2 = models.CharField(max_length = 20)
	op3 = models.CharField(max_length = 20)
	op4 = models.CharField(max_length = 20)
	exam = models.ForeignKey(Exam, on_delete = models.CASCADE)
	
	def __str__(self):
		return self.question

class Messages(models.Model):
	msg = models.TextField()


 
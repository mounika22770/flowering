from django.shortcuts import render
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.urls import reverse

from . models import Student, Exam
from django.views.generic import (
	ListView,
	DetailView,
	CreateView,
	FormView,
)

def home(request):
	context = {
	    'exams': Exam.objects.all()
	}
	return render(request, 'map/home.html', context)

class ExamListView(ListView): #class based views
 	model = Exam
 	template_name = 'map/home.html'  #<app>/<model>_<viewtype>.html
 	context_object_name = 'exams'
 	ordering = ['subject'] #['-exam']


class ExamDetailView(DetailView):
 	model = Exam


class ExamCreateView(CreateView):
	model = Exam
	#template_name = 'map/exam_form.html'
	fields = ['subject']

class ExamFormView(FormView):
	model = Exam 



def login_student(request):
	return render(request, 'map/login.html')

def register_student(request):
	context = {"title": 'Register'}
	return render(request, 'map/register.html', context)

def add_student(request):
	name = request.POST['name']
	roll = request.POST['roll']
	try:
		student = Student.nodes.get(roll = roll, name = name)
	except:
		Student(roll = roll, name = name).save()
	else:
		return HttpResponse("You are already registered. Please Login")
	return HttpResponse("You're successfully registered")
	#return HttpResponseRedirect(reverse("You are registered"))

def front(request):
	return render(request, 'map/front.html')

def display_results(request):
	pass

# def add_question(request):
# 	subject = request.POST["subject"]
# 	question = request.POST["question"]
# 	op1 = request.POST["op1"]
# 	op2 = request.POST["op1"]
# 	op3 = request.POST["op1"]
# 	op4 = request.POST["op1"]
# 	exam = Exam(subject= subject)
	


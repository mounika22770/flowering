from django.db import models
from neomodel import StructuredNode
from neomodel import StringProperty
from django_neomodel import DjangoNode
from django.shortcuts import reverse

from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _
from django.contrib.auth.models import User



class Student(DjangoNode):
	# MALE = 'M'
	# FEMALE = 'F'
	# GENDER = [
	#     (MALE, 'Male'),
	#     (FEMALE, 'Female')

	# ]
	name = StringProperty(required = True, max_length = 50)
	roll = StringProperty(unique_index = True, required = True, max_length = 10)
	# gender = models.CharField(max_length = 2, choices = GENDER)
	#marks = {}

	def __init__(self, name, roll):
		self.name = name
		self.roll = roll

	def __str__(self):
		return self.name


class Teacher(DjangoNode):
	name = StringProperty(required = True, max_length = 50)
	#password = null

	def __init__(self, name):
		self.name = name

	def __str__(self):
		return self.name

def validate_subject(value):
	if not value.isalnum():
		raise ValidationError(_('%(value)s is not valid. Please use alphanumneric characters as subject names'), params={'value': value},)



class Exam(models.Model):  #Exam can have many questions
	subject = models.CharField(primary_key=True, unique = True, validators = [validate_subject], max_length = 20)  #make to reject a string of length 0
	#subject = models.TextField(unique = True)
	set_by = models.ForeignKey(User, on_delete = models.DO_NOTHING)
	#start = 
	#end =  
	#duration = 
	# def __init__(self, name):  'Question' object has no attribute '_state'
	# 	self.subject = name

	def __str__(self):
		return self.subject

	# def get_absolute_url(self):
	# 	return "/exam-detail/%s/" % self.pk


class Question(models.Model):
	exam = models.ForeignKey(Exam, on_delete = models.CASCADE) #deleting an exam deletes all questions
	question = models.TextField(unique = True)
	#skill = models.TextField()
	#answer = models.TextField()		#self.answer = answer

	def __str__(self):
		return self.question
		

class Option(models.Model):
	question = models.ForeignKey(Question, on_delete = models.CASCADE) #deleting a question deletes the correspondent options #handle Integrity Error
	option = models.TextField()
	answer = models.BooleanField(default = False)

	def __str__(self):
		return self.option
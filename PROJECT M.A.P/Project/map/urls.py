from django.urls import path, register_converter

from . import views
from .views import (
	ExamListView,
	ExamDetailView,
	ExamCreateView,
    ExamFormView,
)

app_name='map'
urlpatterns = [
    path("", ExamListView.as_view(), name='map-home'),
    path("exam/form/", ExamFormView.as_view(), name="exam-form"),
    path("exam/new/", ExamCreateView.as_view(), name="exam-create"),
    path("exam/<str:pk>/", ExamDetailView.as_view(), name="exam-detail"),
    path("login_student/", views.login_student, name='map-login'),
    path("register_student", views.register_student, name='map-register'),
    path('add_student/', views.add_student, name='add_student'),
    path('front/', views.front, name="front"),
    path('results/', views.display_results, name="show-results"),
]
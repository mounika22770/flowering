# -*- coding: utf-8 -*-
"""
Created on Wed Feb 12 11:45:41 2020

@author: Mounika
"""

from neo4j import GraphDatabase

class Graph(object):
    
    def __init__(self, uri, user, password):
        self._driver = GraphDatabase.driver(uri, auth=(user, password))
    
    def close(self):
        self._driver.close()
    
    def print_node(self, message):
        with self._driver.session() as session:
            node = session.write_transaction(self._create_and_return_node, message)
            print(node)
    
    @staticmethod
    def _create_and_return_node(tx, message):
        print(type(tx))
        result = tx.run("CREATE (a:Person {name: $message})  SET a.message = $message  RETURN a.message + ', from node ' + id(a)", message = message)
        return result.single()[0]   
                                            
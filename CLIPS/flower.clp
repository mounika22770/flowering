;flower guide system

(deftemplate Flower(slot shape) (slot color) (slot size) (slot fragrance))


(defrule cleome-viscosa
	(Flower (color yellow) (size tiny) (fragrance no) (shape unknown))
	=>
	(printout t "The flower might be Cleome Viscosa" crlf))

(defrule pin-wheel
	(Flower (shape pin-wheel) (color white) (fragrance no) (size small))
	=>
	(printout t "The flower is Tabernaemontana divaricata. Commonly called as crape jasmine" crlf))

(defrule fire-crackers 
	 (Flower (shape fan) (color orange | green | yellow) (size small) (fragrance no)) 
	 =>
	 (printout t "The flower is Crossandra infundibuliformis. Common name: Fire crackers
	(Kanakambaram)" crlf))

(defrule yellow-trumpet 
	 (Flower (shape trumpet) (color yellow) (size small) (fragrance no)) 
	 =>
	 (printout t "The flower might be Tecoma stans.
	(Kanakambaram)" crlf))

(defrule cork-tree
	 (Flower (fragrance yes) (color white) (size big))
	=>
	(printout t "The flower is Millingtonia hortensis. Common name: Cork tree flower" crlf))

(defrule enter_details =>
	(printout t "Please enter the shape (trumpet, fan, pin-wheel, unknown)" crlf)
	(bind ?the_shape (read))
	(printout t "Enter the size (tiny(<2cm), small(2cm-5cm), medium(5cm-10cm), large(10cm-20cm), very-large(>20cm))" crlf)
	(bind ?the_size (read))
	(printout "Does it have fragrance (yes, no)" crlf)
	(bind ?frag (read))
	(printout "What's the color (white, green, yellow, orange, red, pink, blue)" crlf)
	(bind ?c (read))
	(assert (Flower (size ?the_size) (shape ?the_shape) (fragrance ?frag) (color ?c))))





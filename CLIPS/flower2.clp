;flower guide system

(deftemplate Shape(slot Fshape))
(deftemplate Color(slot Fcolor))
(deftemplate Size(slot Fsize))
(deftemplate Fragrance(slot Ffragrance))


(defrule cleome-viscosa
	(Color (Fcolor  yellow))
	(Size (Fsize tiny))
	(Fragrance (Ffragrance no))
	(Shape (Fshape unknown))
	=>
	(printout t "The flower might be Cleome Viscosa" crlf))

(defrule pin-wheel
	(Color (Fcolor white))
	(Size (Fsize small))
	(Fragrance (Ffragrance no))
	(Shape (Fshape pin-wheel))
	=>
	(printout t "The flower is Tabernaemontana divaricata. Commonly called as crape jasmine" crlf))

(defrule fire-crackers 
	(Color (Fcolor orange | green | yellow))
	(Size (Fsize small))
	(Fragrance (Ffragrance no))
	(Shape (Fshape fan))
	=>
	 (printout t "The flower is Crossandra infundibuliformis. Common name: Fire crackers
	(Kanakambaram)" crlf))

(defrule yellow-trumpet 
	(Color (Fcolor yellow))
	(Size (Fsize small))
	(Fragrance (Ffragrance no))
	(Shape (Fshape trumpet))
	 =>
	 (printout t "The flower might be Tecoma stans." crlf))

(defrule cork-tree
	(Color ( Fcolor white))
	(Size (Fsize big))
	(Fragrance (Ffragrance no))
	(Shape (Fshape tube))
	=>
	(printout t "The flower is Millingtonia hortensis. Common name: Cork tree flower" crlf))

(defrule nerium
	(Color (Fcolor pink | red | white))
	(Size (Fsize big))
	(Fragrance (Ffragrance yes))
	(Shape (Fshape Funnel ))
	=>
	(printout t "T")
)


(defrule enter_details =>
	(printout t "Please enter the shape (trumpet, fan, pin-wheel, unknown)" crlf)
	(bind ?the_shape (read))
	(printout t "Enter the size (tiny(<2cm), small(2cm-5cm), medium(5cm-10cm), large(10cm-20cm), very-large(>20cm))" crlf)
	(bind ?the_size (read))
	(printout "Does it have fragrance (yes, no)" crlf)
	(bind ?frag (read))
	(printout "What's the color (white, green, yellow, orange, red, pink, blue)" crlf)
	(bind ?c (read))
	(assert (Size (Fsize ?the_size)) (Shape (Fshape ?the_shape)) (Fragrance (Ffragrance ?frag)) (Color (Fcolor ?c))))





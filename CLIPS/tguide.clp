;traffic system

(deftemplate signal (slot light))


(defrule red-light
(signal (light red))
=>
(printout t "Stop" crlf))

(defrule green-light
(signal (light green))
=>
(printout t "Go" crlf))

(defrule orange-light (signal light orange) => (printout t "Get Ready To Move"))

(defrule enter_light =>
	(printout t "Please enter the sign (red, green, orange, or
unknown)" crlf)
	(bind ?the_color (read))
	(assert (signal (light ?the_color))))
